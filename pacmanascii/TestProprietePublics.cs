﻿using System;

namespace pacmanascii
{
    class TestProprietePublic : AbstractTest
    {
        public TestProprietePublic (string name) : base(name) {}

        override public bool test() 
        {
            ProprietePublic cp = new ProprietePublic(42);
            cp.name = 42;
            if (cp.name == 42) {
                return true;
            } else {
                return false;
            }
        }
    }
}
