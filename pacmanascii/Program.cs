﻿using System;

namespace pacmanascii
{
    class Program 
    {
        static void Main(string[] args)
        {
            foreach (string s in args) 
            {
                switch (s)
                {
                    case "testchampspublic":
                        TestChampsPublic tcp = new TestChampsPublic("Test Champs Public");

                        tcp.Main(new string[0]);
                        break;
                    case "testproprietepublics":
                        TestProprietePublic tcp2 = new TestProprietePublic("Test Propriete Public");

                        tcp2.Main(new string[0]);
                        break;

                    case "testsumnentiers":
                        TestSumNEntiers tcp3 = new TestSumNEntiers("Test Somme n Entiers");

                        tcp3.Main(new string[0]);
                        break;
                    
                    default:
                        break;
                }
            }
        }
    }
}
