﻿using System;

namespace pacmanascii{

   public class ProprietePublic
   {
      private int _pp;
  
      public ProprietePublic(int name)
      {
         _pp = name;
      }

      public int name
      {
         get => _pp;
         set => _pp = value;
      }

   
   }
}