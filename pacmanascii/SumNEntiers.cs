﻿using System;

namespace pacmanascii
{
    public class SumNEntiers
    {   
        private int N;
        public SumNEntiers (int name)
        {
            N = name;
        }

        public int name
        {
            get => N;
            set => N = value;
        }

    }
}